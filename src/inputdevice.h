#pragma once

#include <cstdint>

class InputDevice {
	private:
		void fetch(void);
	public:
		int16_t mouse_x, mouse_y;
		bool mouse_enabled;
		InputDevice();
		~InputDevice();
		void update(void);
		void set_mouse_pos(int16_t x, int16_t y);
		bool key_down(uint32_t key);
		bool key_pressed(uint32_t key);
		bool key_released(uint32_t key);
		bool mouse_pressed(uint8_t button); // 0 = nothing, 1 == left, 2 == right, 3 == middle
};
