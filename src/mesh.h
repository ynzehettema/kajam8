#pragma once

#include <vector>
#include <glm/vec3.hpp>
#include "face.h" 

struct Mesh
{
  glm::vec3 position;
  glm::vec3 rotation;
  std::vector<glm::vec3> vertices;
  std::vector<Face> faces;
};
