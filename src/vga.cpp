#include "vga.h"

#include "dpmi.h"
#include "pc.h"
#include <sys/nearptr.h>

#include <stdlib.h>
#include <string.h>


namespace VGA 
{
  int VGA_STRIDE = 320,
      VGA_HEIGHT = 200;

  uint32_t vgastart = 0xA0000;
  uint8_t* double_buffer;

  void init()
  {
     setmode(0x13); 
     vgastart = (uint32_t)(vgastart + __djgpp_conventional_base);

     double_buffer = (uint8_t*)malloc(VGA_STRIDE*VGA_HEIGHT);
     for(int i = 0; i < VGA_STRIDE*VGA_HEIGHT; ++i)
       double_buffer[i] = 0;
  }
  void exit()
  {
    setmode(0x3);

    free(double_buffer);
  }

  void setmode(uint32_t mode)
  {
    __dpmi_regs r;
    r.x.ax = mode;
    __dpmi_int(0x10, &r);
  }

  void palette(uint8_t color, uint8_t r, uint8_t g, uint8_t b)
  {
    // VGA DAC address register: 0x3C8
    // VGA DAC data register: 0x3C9
    outportb(0x3C8, color);
    outportb(0x3C9, r);
    outportb(0x3C9, g);
    outportb(0x3C9, b);
  }

  void pixel(int x, int y, uint8_t color)
  {
    *(double_buffer + (y * VGA_STRIDE) + x) = color;
    /*
    uint8_t volatile *vgaptr = (uint8_t volatile *)(vgastart);
    vgaptr += (y * VGA_STRIDE) + x;
    *vgaptr = color;
    */
  }

  void fill(uint8_t color) 
  {
     for(int i = 0; i < VGA_STRIDE*VGA_HEIGHT; ++i)
       double_buffer[i] = color;
  }

  void draw()
  {
    memcpy((uint8_t*)vgastart, double_buffer, VGA_STRIDE*VGA_HEIGHT);

    // vertical sync
    do{}
    while(!(inportb(0x3DA) & 8));
  }
}
