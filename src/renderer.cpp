#pragma once

#include <vector>
#include <limits>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "vga.h"
#include "mesh.h"
#include "camera.h"

namespace Renderer
{
  float* zbuffer;

  void clear()
  {
    for(int i = 0; i < VGA::VGA_STRIDE * VGA::VGA_HEIGHT; ++i)
      zbuffer[i] = std::numeric_limits<float>::max();
  }

  glm::vec3 project(glm::vec3 const coord, glm::mat4 const matrix)
  {
    auto point = glm::vec3(matrix * glm::vec4(coord, 1.0));

    return glm::vec3(point.x * (float)VGA::VGA_STRIDE + (float)VGA::VGA_STRIDE / 2.0f,
                    -point.y * (float)VGA::VGA_STRIDE + (float)VGA::VGA_HEIGHT / 2.0f,
                     coord.z);
  }

  void draw_point(glm::vec2 point, float z, uint8_t color)
  {
    if(point.x >= 0 && point.y >= 0 &&
       point.x < VGA::VGA_STRIDE &&
       point.y < VGA::VGA_HEIGHT)
    {
      if(zbuffer[(int)point.y * VGA::VGA_STRIDE + (int)point.x] < z)
        return;

      zbuffer[(int)point.y * VGA::VGA_STRIDE + (int)point.x] = z;

      VGA::pixel((int)point.x, (int)point.y, color);
    }
  }

  void draw_line(glm::vec2 const& point0, glm::vec2 const& point1, uint8_t color)
  {
    auto dist = (point1 - point0).length();

    if(dist < 1)
      return;

    for(float i = 0.01f; i < 1.0f; i += 0.01f)
    {
      auto n = point0 + (point1 - point0) * i;
      draw_point(n, 0, color);
    }
  }
  
  inline float clamp(float value, float min = 0.0f, float max = 1.0f)
  {
    if(value > max)
      return max;
    if(value < min)
      return min;

    return value;
  }
  inline float interpolate(float min, float max, float gradient)
  {
    return min + (max - min) * clamp(gradient);
  }

  void draw_bline(glm::vec2 const& point0, glm::vec2 const& point1, uint8_t color)
  {
    int x0 = (int)point0.x;
    int y0 = (int)point0.y;
    int x1 = (int)point1.x;
    int y1 = (int)point1.y;
            
    auto dx = std::abs(x1 - x0);
    auto dy = std::abs(y1 - y0);
    auto sx = (x0 < x1) ? 1 : -1;
    auto sy = (y0 < y1) ? 1 : -1;
    auto err = dx - dy;

    while(true)
    {
      draw_point(glm::vec2(x0,y0), 0, color);

      if ((x0 == x1) && (y0 == y1)) break;
      auto e2 = 2 * err;
      if (e2 > -dy) { err -= dy; x0 += sx; }
      if (e2 < dx) { err += dx; y0 += sy; }
    }

  }


  void scanline(int y, glm::vec3 const& pa, glm::vec3 const& pb, glm::vec3 const& pc, glm::vec3 const& pd, uint8_t color)
  {
    auto gradient1 = pa.y != pb.y ? (y - pa.y) / (pb.y - pa.y) : 1;
    auto gradient2 = pc.y != pd.y ? (y - pc.y) / (pd.y - pc.y) : 1;

    int sx = (int)interpolate(pa.x, pb.x, gradient1);
    int ex = (int)interpolate(pc.x, pd.x, gradient2);

    float z1 = interpolate(pa.z, pb.z, gradient1);
    float z2 = interpolate(pc.z, pd.z, gradient2);

    for(int x = sx; x < ex; ++x)
    {
      float gradient = (x - sx) / (float)(ex - sx);

      float z = interpolate(z1, z2, gradient);
      draw_point(glm::vec2(x,y), z, color);
    }
  }

  void draw_triangle(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, uint8_t color)
  {
    if (p1.y > p2.y)
    {
        auto temp = p2;
        p2 = p1;
        p1 = temp;
    }

    if (p2.y > p3.y)
    {
        auto temp = p2;
        p2 = p3;
        p3 = temp;
    }

    if (p1.y > p2.y)
    {
        auto temp = p2;
        p2 = p1;
        p1 = temp;
    }

    // inverse slopes
    float dp1p2, dp1p3;

    // http://en.wikipedia.org/wiki/slope
    // computing inverse slopes
    if (p2.y - p1.y > 0)
        dp1p2 = (p2.x - p1.x) / (p2.y - p1.y);
    else
        dp1p2 = 0;

    if (p3.y - p1.y > 0)
        dp1p3 = (p3.x - p1.x) / (p3.y - p1.y);
    else
        dp1p3 = 0;

    // first case where triangles are like that:
    // p1
    // -
    // -- 
    // - -
    // -  -
    // -   - p2
    // -  -
    // - -
    // -
    // p3
    if (dp1p2 > dp1p3)
    {
        for (auto y = (int)p1.y; y <= (int)p3.y; y++)
        {
            if (y < p2.y)
            {
                scanline(y, p1, p3, p1, p2, color);
            }
            else
            {
                scanline(y, p1, p3, p2, p3, color);
            }
        }
    }
    // first case where triangles are like that:
    //       p1
    //        -
    //       -- 
    //      - -
    //     -  -
    // p2 -   - 
    //     -  -
    //      - -
    //        -
    //       p3
    else
    {
        for (auto y = (int)p1.y; y <= (int)p3.y; y++)
        {
            if (y < p2.y)
            {
                scanline(y, p1, p2, p1, p3, color);
            }
            else
            {
                scanline(y, p2, p3, p1, p3, color);
            }
        }
    }
  }

  void render(Camera const& camera, std::vector<Mesh> const& meshes)
  {
    auto view = glm::lookAtLH(camera.position, camera.target, glm::vec3(0.0f, 1.0f, 0.0f));
    //auto projection = glm::perspective(0.66f, 1.6f, 0.01f, 10.0f);
    auto projection = glm::mat4(1.0f);

    for(auto& mesh: meshes)
    {
      glm::mat4 world = glm::mat4(1.0f);
      world = glm::scale(world, glm::vec3(0.1f, 0.1f, 0.1f));
      world = glm::translate(world, mesh.position);
      world = glm::rotate(world, mesh.rotation.x, glm::vec3(1,0,0));
      world = glm::rotate(world, mesh.rotation.y, glm::vec3(0,1,0));
      world = glm::rotate(world, mesh.rotation.z, glm::vec3(0,0,1));

      auto transform = projection * view  * world;

      /*
      for(auto& vertex: mesh.vertices)
      {
        auto point = project(vertex, transform);

        draw_point(point, 0, 15);
      }*/

      /*
      for(int i = 0; i < mesh.vertices.size() - 1; ++i)
      {
        auto point0 = project(mesh.vertices.at(i), transform);
        auto point1 = project(mesh.vertices.at(i + 1), transform);

        draw_line(point0, point1);
      }*/

      /*
      for(auto& face: mesh.faces)
      {
        auto vertexa = mesh.vertices[face.a];
        auto vertexb = mesh.vertices[face.b];
        auto vertexc = mesh.vertices[face.c];

        auto pixela = project(vertexa, transform);
        auto pixelb = project(vertexb, transform);
        auto pixelc = project(vertexc, transform);

        draw_bline(pixela, pixelb);
        draw_bline(pixelb, pixelc);
        draw_bline(pixelc, pixela);
      }*/

      int i = 0;
      for(auto& face: mesh.faces)
      {
        auto vertexa = mesh.vertices[face.a];
        auto vertexb = mesh.vertices[face.b];
        auto vertexc = mesh.vertices[face.c];

        auto pixela = project(vertexa, transform);
        auto pixelb = project(vertexb, transform);
        auto pixelc = project(vertexc, transform);

        
        draw_triangle(pixela, pixelb, pixelc, i);

        ++i;
      }
    }
  }
}

