#pragma once

#include <cstdint>

namespace VGA
{
  extern int VGA_STRIDE,
             VGA_HEIGHT;
  extern uint32_t vgastart;
  extern uint8_t* double_buffer;

  void init();
  void exit();
  
  void setmode(uint32_t mode); 

  void palette(uint8_t color, uint8_t r, uint8_t g, uint8_t b);

  void pixel(int x, int y, uint8_t color);
  void fill(uint8_t color);

  void draw();
}
