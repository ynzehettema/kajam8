#include <stdio.h>
#include <cstdint>

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>

#include <dirent.h>

using namespace std;

static FILE *out;

vector<string> get_files(const char *d) {
	vector<string> ret;
	
	DIR* dir = opendir(d);
	struct dirent * dp;
	while((dp = readdir(dir)) != NULL)
		ret.push_back(dp->d_name);
	closedir(dir);

	return ret;
}

uint32_t filesize(const char *f) {
	FILE *fp = fopen(f, "rb");
	fseek(fp, 0, SEEK_END);
	uint32_t ret;
	ret = ftell(fp);
	fclose(fp);
	return ret;
}

bool ends_with(string a, string b) {
	if(b.size() > a.size()) return false;
	if(a.compare(a.size() - b.size(), b.size(), b) == 0) return true;
	std::transform(a.begin(), a.end(), a.begin(), ::tolower);
	std::transform(b.begin(), b.end(), b.begin(), ::tolower);
	if(a.compare(a.size() - b.size(), b.size(), b) == 0) return true;
	return false;
}

// packs a pcx file, but only the bits the game actually needs
// that is: the file header and pixel data.
// the palette is omitted to save space
// returns the length of the file
vector<uint8_t> pack_pcx(string &f) {
	printf("\tWriting: %s", f.c_str());	

	vector<uint8_t> ret;

	FILE *fp = fopen(f.data(), "rb");
	
	if(fp == NULL) {
		fprintf(stderr, "Could not open: %s for writing. \n", f.c_str());
		return ret;
	}
	
	fseek(fp, 4, SEEK_SET);
	
	// xmin ymin maxx maxy
	uint16_t xm,ym,mx,my, width, height; 
	
	xm = fgetc(fp);
	xm = xm | fgetc(fp) << 8;
	ym = fgetc(fp);
	ym = ym | fgetc(fp) << 8;
	mx = fgetc(fp);
	mx = mx | fgetc(fp) << 8;
	my = fgetc(fp);
	my = my | fgetc(fp) << 8;
	
	width = mx - xm + 1;
	height = my - ym + 1;

	printf("\t-> %dx%d ", width, height);

	ret.push_back((uint8_t)(width >> 8));
	ret.push_back((uint8_t)width);
	ret.push_back((uint8_t)(height >> 8));
	ret.push_back((uint8_t)height);

	fseek(fp, 128, SEEK_SET);

	uint32_t i = 0;
	uint8_t b, c;
	do {
		b = fgetc(fp);

		if(b & 0xC0) { // check for RLE		
			ret.push_back(b);

			b = b & 0x3F; // remove RLE signature to get run length
			c = fgetc(fp); // get color
			ret.push_back(c);

			i += b;
		} else {
			ret.push_back(b);
			++i;
		}
	} while(i < width * height);

	printf("%d/%d", i, width*height);
	
	fclose(fp);

	return ret;
}

vector<uint8_t> pack_palette(string &f) {
	printf("\tWriting: %s", f.c_str());	

	vector<uint8_t> ret;

	FILE *fp = fopen(f.data(), "rb");
	
	if(fp == NULL) {
		fprintf(stderr, "Could not open: %s for writing. \n", f.c_str());
		return ret;
	}

	fseek(fp, -768, SEEK_END);
	for(uint16_t i = 0; i < 768; ++i)	
		ret.push_back(fgetc(fp));

	fclose(fp);
	printf("\t-> done\t\t");


	return ret;
}

vector<uint8_t> pack_file(string &f) {
	printf("\tWriting: %s", f.c_str());

	vector<uint8_t> ret;

	FILE *fp = fopen(f.data(), "rb");
	
	if(fp == NULL) {
		fprintf(stderr, "Could not open: %s for writing. \n", f.c_str());
		return ret;
	}
	fseek(fp, 0, SEEK_END);
	int16_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	for(uint16_t i = 0; i < size; ++i)	
		ret.push_back(fgetc(fp));

	fclose(fp);
	printf("\t-> done\t\t");

	return ret;
}

void write_file(string &file, string &name) {
	string 	pcx_extension = ".pcx",
					rad_extension = ".rad",
					map_extension = ".map",
					wav_extension = ".wav",
					palette = "palette.pcx";

	uint32_t original_size, new_size;
	original_size = filesize(file.c_str());

	vector<uint8_t> data;

	if(ends_with(file, palette)) {
		auto paldata = pack_palette(file);
		data.insert(data.end(), paldata.begin(), paldata.end());

		new_size = paldata.size();
	}	else if(ends_with(file, pcx_extension)) {
		auto pcxdata = pack_pcx(file);
		data.insert(data.end(), pcxdata.begin(), pcxdata.end());

		new_size = pcxdata.size() + 3 + name.size();
	}	else if(ends_with(file, rad_extension) || ends_with(file, map_extension) || 
						ends_with(file, wav_extension)) { // raw file packing
		auto rawdata = pack_file(file);
		data.insert(data.end(), rawdata.begin(), rawdata.end());

		new_size = rawdata.size();
	}
	if(data.size() == 0) {
		printf("\tNot packing: %s\t-> unknown extension\n", file.c_str());
		return;
	}

	fputc((uint8_t)name.size(), out); // name length
	fputs(name.c_str(), out); // name of file

	fputc((uint8_t)(data.size() >> 8), out); // data length
	fputc((uint8_t)data.size(), out); // data length

	for(uint16_t i = 0; i < data.size(); i++)
		fputc(data[i], out);

	float osf = original_size;
	printf("\t%d -> %d bytes (%d%% savings)\n", original_size, new_size, int(100 - (new_size / osf * 100)));
}

int main(int argc, char *argv[]) {
	if(argc < 2) {
		fprintf(stderr, "packer expects the name of the data file to pack\n");
		return 1;
	}

	const char *asset_path = "assets/",
						 *out_file = argv[1];

	auto files = get_files(asset_path);	

	out = fopen(out_file, "wb");
	
	printf("Packing: %s\n", out_file);

	if(out == NULL) {
		fprintf(stderr, "Could not open output file");
		return 1;
	}

	// Alphabetical sorting so game loads assets in the right order
	std::sort(files.begin(), files.end());

	for(auto& name : files) {
		if(name[0] == '.') continue; // we're not packing . or ..

		string f(asset_path);
		f.append(name);
		write_file(f, name);
	}

	fclose(out);
	printf("%s is packed.\n", out_file);
	return 0;	
}
